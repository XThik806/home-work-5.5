import React from 'react';
import { Route, Routes } from 'react-router-dom';
import ProductContainer from './components/ProductContainer';
import CartContainer from './Routes/CartContainer';
import FavoriteContainer from './Routes/FavoriteContainer';

const AppRouter = ({ products, addToFavorite, isFavorite, modalActivate, setCurrentProduct, cartItems = [], removeCart = () => {}, favorite }) => {
    return (
        <Routes>
            <Route path='/' element={<ProductContainer products={products} addToFavorite={addToFavorite} isFavorite={isFavorite} modalActivate={modalActivate} setCurrentProduct={setCurrentProduct}/>} />
            <Route path='/cart' element={<CartContainer cartItems={cartItems} removeCart={removeCart} ></CartContainer>} />
            <Route path='/favorite' element={<FavoriteContainer favorite={favorite} addToFavorite={addToFavorite} ></FavoriteContainer>}></Route>
        </Routes>
    );
}

export default AppRouter;
