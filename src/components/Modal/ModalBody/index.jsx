import React from 'react';
import styles from './ModalBody.module.scss'

const ModalBody = ({modalActivate, curentProduct, addToCart}) => {
    return (
        <div className={styles.body}>
            <div className={styles.wrapper} onClick={modalActivate}>
                <div className={styles.modal} onClick={(event) => {event.stopPropagation()}}>
                    <span>Are you sure you would like to add to cart {curentProduct.title} book?</span>
                    <div className={styles.buttons}>
                        <button className={styles.buttonYes} onClick={() => {
                            addToCart(curentProduct);
                            modalActivate();
                        }}>Yes, add to cart</button>
                        <button className={styles.buttonNo} onClick={modalActivate}>No, back to list</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModalBody;
