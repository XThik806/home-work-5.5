import React from 'react';
import starIcon from './starIcon.svg'

const StarIcon = () => {
    return (
        <img src={starIcon} width={20} height={20} alt="starIcon" />
    );
}

export default StarIcon;
